var searchData=
[
  ['hcsr04deinit',['HcSr04Deinit',['../hc__sr4_8h.html#a8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../hc__sr4_8c.html#a8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init',['HcSr04Init',['../hc__sr4_8h.html#a9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../hc__sr4_8c.html#a9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters',['HcSr04ReadDistanceCentimeters',['../hc__sr4_8h.html#a30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../hc__sr4_8c.html#a30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches',['HcSr04ReadDistanceInches',['../hc__sr4_8h.html#ab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../hc__sr4_8c.html#ab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]]
];
