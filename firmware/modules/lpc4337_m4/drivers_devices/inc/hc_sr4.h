/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Albano Peñalva
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "bool.h"
#include "gpio.h"
/*==================[external functions declaration]=========================*/


//Comentarios arriba de cada función.
bool HcSr04Init(gpio_t echo, gpio_t trigger);
uint16_t HcSr04ReadDistanceCentimeters(void);
uint16_t HcSr04ReadDistanceInches(void);
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);



/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

