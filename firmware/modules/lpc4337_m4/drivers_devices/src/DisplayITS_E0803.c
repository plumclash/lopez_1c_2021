///*! @mainpage Blinking
// *
// * \section genDesc General Description
// *
// * This application makes the led blink
// *
// * \section hardConn Hardware Connection
// *
// * | Dispositivo1	|   EDU-CIAA	|
// * |:--------------:|:--------------|
// * | 	PIN1	 	| 	GPIO3		|
// *
// *
// * @section changelog Changelog
// *
// * |   Date	    | Description                                    |
// * |:----------:|:-----------------------------------------------|
// * | 01/01/2020 | Document creation		                         |
// * |			|							                     |
// *
// * @author Albano Peñalva
// *
// */
//
///*==================[inclusions]=============================================*/
#include "../inc/DisplayITS_E0803.h"       /* <= own header */
#include "gpio.h"
#include "stdint.h"
#include <stdio.h>
#include "math.h"
//
/*==================[macros and definitions]=================================*/


#define MASC 0X01
uint16_t NumeroElegido;
gpio_t BCD_gpio[4];	// 4 BCD
gpio_t OrdenSeleccion[3];	//decena centena unidad
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


void  ConversorUnDecCen (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t aux;
	uint8_t i;

	for ( i = 0; i < digits; i++){
		aux = data % 10;
		bcd_number[digits-i-1] = aux;
		data -= aux;
		data = data/10;
	}}  //Convierte un numero de tres cifras en un arreglo de 3 numeros separados
//	(unidad - decena - centena)


//
//	void gpio_tToBCD( uint8_t BCD){
//		uint8_t i;
//		for (i = 0; i < 4; i++){
//		BCD_gpio = (BCD >> i )& MASC;
//		} //convierte un BCD en 4 digitos binarios

}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

bool ITSE0803Init(gpio_t * pins) {
	BCD_gpio[0] = pins[0];
		BCD_gpio[1] = pins[1];
		BCD_gpio[2] = pins[2];
		BCD_gpio[3] = pins[3];

		OrdenSeleccion[0] = pins[4];
		OrdenSeleccion[1] = pins[5];
		OrdenSeleccion[2] = pins[6];

		GPIOInit(pins[0], GPIO_OUTPUT);
		GPIOInit(pins[1], GPIO_OUTPUT);
		GPIOInit(pins[2], GPIO_OUTPUT);
		GPIOInit(pins[3], GPIO_OUTPUT);
		GPIOInit(pins[4], GPIO_OUTPUT);
		GPIOInit(pins[5], GPIO_OUTPUT);
		GPIOInit(pins[6], GPIO_OUTPUT);



		return 1;
	     	}

}
bool ITSE0803DisplayValue(uint16_t valor){
	uint8_t dcu[3];  /*decena centena unidad*/
	NumElegido = valor;
	//if(0 <= valor <= 999){

		ConversorUnDecCen(valor, 3, dcu);

		graficarBCD(dcu[0]);
		GPIOOn(OrdenSeleccion[0]);
		GPIOOff(OrdenSeleccion[0]);


		graficarBCD(dcu[1]);
		GPIOOn(OrdenSeleccion[1]);
		GPIOOff(OrdenSeleccion[1]);


		graficarBCD(dcu[2]);
		GPIOOn(OrdenSeleccion[2]);
		GPIOOff(OrdenSeleccion[2]);


//		return true;
//	}
//	else
//		return false;
}


uint16_t ITSE0803ReadValue(void){
	return NumeroElegido;

}

bool ITSE0803Deinit(gpio_t * pins){
	return 1;
}

//int main(void){
//
//    while(1){
//
//	}
//
//	return 0;
//}

/*==================[end of file]============================================*/

