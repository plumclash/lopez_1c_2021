/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"       /* <= own header */
#include "delay.h"

/*==================[macros and definitions]=================================*/
//Algun define, typedef, enum
/*==================[internal data definition]===============================*/
//se pueden comentar
gpio_t puerto_de_echo;
gpio_t puerto_de_trigger;

/*==================[internal functions declaration]=========================*/
//Funcion local, con comentarios

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	puerto_de_echo = echo;
	puerto_de_trigger = trigger;

	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);



	return 1;

}


uint16_t HcSr04ReadDistanceCentimeters(void)
{
	GPIOOn(puerto_de_trigger);
	DelayUs(10);
	GPIOOff(puerto_de_trigger);


	uint16_t distancia_cm = 0;

	while(GPIORead(puerto_de_echo)== 0)
	{

	}

	while(GPIORead(puerto_de_echo)== 1)
	{
		DelayUs(1);
		distancia_cm++;
	}

	distancia_cm  = distancia_cm /28;


	return distancia_cm;
}
uint16_t HcSr04ReadDistanceInches(void)
{
	GPIOOn(puerto_de_trigger);
	DelayMs(10);
	GPIOOff(puerto_de_trigger);


	uint16_t distancia_in = 0;

	while(GPIORead(puerto_de_echo)== 0)
	{

	}

	while(GPIORead(puerto_de_echo)== 1)
	{
		DelayUs(1);
		distancia_in++;
	}

	distancia_in  = distancia_in / 60;


	return distancia_in;
}
bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{

	return 1;
}

/*==================[internal functions definition]=========================*/


/*==================[end of file]============================================*/

