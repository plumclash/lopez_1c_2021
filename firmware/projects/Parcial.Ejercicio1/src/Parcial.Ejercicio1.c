/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Se realiza el ejercicio 1, el cual  es un sistema de control de alarma
 *
 *El sistema debe realizar mediciones de distancia a razón de 10 muestras por segundo, y
 *  utilizar dichas mediciones para calcular la velocidad de los vehículos en m/s
 *  (la dirección de avance de los vehículos es siempre hacia el sensor).
 Los valores de velocidad deben ser enviados a la PC a través del puerto serie, con el formato xx + “ m/s”.

 Se debe además utilizar los LEDs de la EDU-CIAA como señales de advertencia de velocidad:
 velocidad menor a 3m/s: LED3,
 velocidad entre 3m/s y 8m/s: LED2,
 velocidad mayora a 8m/s: LED1.
 Finalmente, el sistema debe poder encenderse y apagarse usando la tecla 1 de la EDU-CIAA.
 Indicar el estado encendido utilizando el LED RGB azul.
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 |Se crea proyecto que contiene ejercicio1        |

 *
 * @author Solana Lopez
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial.Ejercicio1.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"
/*==================[macros and definitions]=================================*/

#define SIZEMUESTRA 10
/*==================[internal data definition]===============================*/
uint16_t muestra[SIZEMUESTRA];
uint16_t contador = 0;
uint16_t tiempoInicial = 0;
uint16_t velocidad = 0;
uint16_t distanciaInicial = 0;
bool mood = false;

/*==================[internal functions declaration]=========================*/
void TomarMuestras(void) {
	if (mood ==true) {
		uint16_t i = 0;
		for (i = 0; i < SIZEMUESTRA; i++) {
			muestra[i] = HcSr04ReadDistanceCentimeters();
		}
		contador++;
	}

}

/* funcion que calcula la velocidad, siendo la variacion de la muestra sobre la variacion del tiempo*/

void CalcularVelocidad(void) {
	uint16_t i = 0;
	for (i = 0; i < SIZEMUESTRA; i++) {
		velocidad = (muestra[i] - distanciaInicial)
				/ (contador - tiempoInicial);
		distanciaInicial = muestra[i];
		tiempoInicial = contador;
		velocidad= velocidad/100; // paso a metro por segundo

	}

}

void Velocidad(void) {
	CalcularVelocidad();
}

void OnOff(void) {
	if (mood ==false) {
		mood = true;

	}

	if (mood ==true) {
		mood = false;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/* Ejercicio 1*/

int main(void) {
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();
	/* Inicializo Timer,cada 1 seg, lee la distancia*/

	timer_config timerA_setup = { TIMER_A, 1000, TomarMuestras };
	TimerInit(&timerA_setup);

	/* Configuro puerto serie*/
	serial_config serial = { SERIAL_PORT_PC, 115200, &Velocidad };

	UartInit(&serial);
	/* Inicializo teclas*/
	SwitchesInit();

	SwitchActivInt(SWITCH_1, OnOff);

	while (1) {

		/* Siempre que este encendido,esta prendido el led azul. y dependiendo velocidad se enciende cada led*/

		if (mood == true) {
			LedOn(LED_RGB_B);
			CalcularVelocidad();
			UartSendString(SERIAL_PORT_PC,UartItoa(velocidad,10));
			    		UartSendString(SERIAL_PORT_PC," m/s \n\r");
					UartSendString(SERIAL_PORT_PC, "\r");
					/* si la velocidad es menor a 3 m, se enciende led3*/
			if (velocidad < 300) {
				LedOn(LED_3);
			}
			/* si la velocidad esta entre 3 m y 8m se enciende led 2*/

			if (velocidad >= 300 && velocidad <= 800) {
				LedOn(LED_2);

			}
			/* si la velocidad es mayor a 8 se enciende led 1*/

			if (velocidad > 800) {
				LedOn(LED_1);
			}

		}

	}

	return 0;
}

/*==================[end of file]============================================*/

