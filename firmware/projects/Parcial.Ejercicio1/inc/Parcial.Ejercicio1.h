/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Se realiza el ejercicio 1, el cual  es un sistema de control de alarma
 *
 *El sistema debe realizar mediciones de distancia a razón de 10 muestras por segundo, y
 *  utilizar dichas mediciones para calcular la velocidad de los vehículos en m/s
 *  (la dirección de avance de los vehículos es siempre hacia el sensor).
 Los valores de velocidad deben ser enviados a la PC a través del puerto serie, con el formato xx + “ m/s”.

 Se debe además utilizar los LEDs de la EDU-CIAA como señales de advertencia de velocidad:
 velocidad menor a 3m/s: LED3,
 velocidad entre 3m/s y 8m/s: LED2,
 velocidad mayora a 8m/s: LED1.
 Finalmente, el sistema debe poder encenderse y apagarse usando la tecla 1 de la EDU-CIAA.
 Indicar el estado encendido utilizando el LED RGB azul.
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 |Se crea proyecto que contiene ejercicio1        |

 *
 * @author Solana Lopez
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

