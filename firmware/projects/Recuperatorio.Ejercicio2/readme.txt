﻿En este proyecto se resuelve el inciso 2 del parcial recuperatorio
 * 	Digitalice una señal de ECG conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de muestreo de 250 Hz.
 * 	Implemente un detector de QRS por umbral y envíe la cadena “QRS detectado” por el puerto serie a la PC cada vez
 * 	 que se encuentra un QRS.
 * 	  El valor del umbral debe ser configurable tanto por comandos series como por las teclas.
 * 	  Utilice la Tecla 1 y el carácter ‘U’ para subir el umbral en pasos de 5 [mV];
 * 	   la Tecla 2 y el carácter ‘D’ disminuye el umbral en pasos de 5 [mV].
 * 	   El rango completo para la variable va de 500 a 900.