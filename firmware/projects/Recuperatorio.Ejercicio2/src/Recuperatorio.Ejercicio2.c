/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * 	En este proyecto se resuelve el inciso 2 del parcial recuperatorio
 * 	Digitalice una señal de ECG conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de muestreo de 250 Hz.
 * 	Implemente un detector de QRS por umbral y envíe la cadena “QRS detectado” por el puerto serie a la PC cada vez
 * 	 que se encuentra un QRS.
 * 	  El valor del umbral debe ser configurable tanto por comandos series como por las teclas.
 * 	  Utilice la Tecla 1 y el carácter ‘U’ para subir el umbral en pasos de 5 [mV];
 * 	   la Tecla 2 y el carácter ‘D’ disminuye el umbral en pasos de 5 [mV].
 * 	   El rango completo para la variable va de 500 a 900.
 *
 *
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Se comienza el proyecto		                 |
 *
 *
 * @author Solana Lopez
 *
 /*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.Ejercicio2.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define UmbralMin 500
#define UmbralMax 900

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

void SistInit(void);
void ConversionAD(void);
void CrucePorCero(void);
void LecturaECG(void);

/*==================[external data definition]===============================*/

/* me pide una frecuencia de muestreo de 250 hz/*/
timer_config my_timer = { TIMER_A, 4, &ConversionAD };
uint16_t valor = 0;
analog_input_config entrada = { CH1, AINPUTS_SINGLE_READ, &LecturaECG };
uint16_t umbral = UmbralMin;

/*==================[external functions definition]==========================*/

void ConversionAD(void) {

	AnalogStartConvertion();

}
void DetectorQRS(void) {

	if (valor == umbral) {
		UartSendString(SERIAL_PORT_PC, " QRS ENCONTRADO \n\r");
	}

}
void LecturaEMG(void) {
	AnalogInputRead(CH1, &valor);
	valor= valor* (3300/1024); // lo paso a mV
	DetectorQRS();

}

void BajarUmbral(void) {
	if (umbral > UmbralMin) {
		umbral = umbral - 5;
	}

	void SubirUmbral(void) {
		if (umbral < UmbralMax) {
			umbral = umbral + 5;
		}
	}

	void DoPuerto(void) {

		uint8_t dato;

		UartReadByte(SERIAL_PORT_PC, &dato);

		if (dato == 'U') {
			SubirUmbral();
		}
		if (dato == 'D') {
			BajarUmbral();
		}

	}

	void SistInit(void) {
		SystemClockInit();
		TimerInit(&my_timer);
		AnalogInputInit(&entrada);
		serial_config medicion = { SERIAL_PORT_PC, 115200, &DoPuerto };
		UartInit(&medicion);

		TimerStart(TIMER_A);
		/* Inicializo teclas*/
		SwitchesInit();
		SwitchActivInt(SWITCH_1, SubirUmbral);
		SwitchActivInt(SWITCH_2, BajarUmbral);
	}

	int main(void) {
		SistInit();
		while (1) {

		}

		return 0;
	}
}

/*==================[end of file]============================================*/

