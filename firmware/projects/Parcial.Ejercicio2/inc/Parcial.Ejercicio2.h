/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 *Se desea realizar un control de fase digital para controlar la velocidad de un motor universal.
 Se necesita digitalizar la senoidal de la red con 1 ms de resolución y detectar el cruce por cero,
 y luego de R0 milisegundos, disparar un triac con un pulso de 1ms.



 Teniendo en cuenta que el CAD del LCP4337 (EDU-CIAA) es monopolar,
 se debe considerar que la señal de entrada es acondicionada por el bloque de la figura obteniedno una excursión de 0 a 3.3 V
 y valor medio 1.65V.

 El valor de R0 debe poder tomar valores de entre 0 y 20 ms con pasos de 5 ms,
 controlado a través de puerto serie con los comandos ‘S’ (subir) y ‘B’ (bajar). cuando presiona  S sube 5
 cuando presiona B baja
 *
 *

 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Se crea proyecto ejercicio2	                 |
 * |			|							                     |
 *
 * @author Solana Lopez
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

