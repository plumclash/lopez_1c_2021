/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 *Se desea realizar un control de fase digital para controlar la velocidad de un motor universal.
 Se necesita digitalizar la senoidal de la red con 1 ms de resolución y detectar el cruce por cero,
 y luego de R0 milisegundos, disparar un triac con un pulso de 1ms.



 Teniendo en cuenta que el CAD del LCP4337 (EDU-CIAA) es monopolar,
 se debe considerar que la señal de entrada es acondicionada por el bloque de la figura obteniedno una excursión de 0 a 3.3 V
 y valor medio 1.65V.

 El valor de R0 debe poder tomar valores de entre 0 y 20 ms con pasos de 5 ms,
 controlado a través de puerto serie con los comandos ‘S’ (subir) y ‘B’ (bajar). cuando presiona  S sube 5
 cuando presiona B baja
 *
 *

 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Se crea proyecto ejercicio2	                 |
 * |			|							                     |
 *
 * @author Solana Lopez
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial.Ejercicio2.h"       /* <= own header */
#include "systemclock.h"
#include "analog_io.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t valor = 0;
uint16_t R0 = 0;

/*==================[internal functions declaration]=========================*/
void TakeSample(void) {
	AnalogInputRead(CH1, &valor);
	if (valor >= 1.65) {
		R0++;
	}

}
void InterrupcionAnalogico(void) {
	AnalogStartConvertion();
}

void DoPuerto(void) {
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	if (dato == 'S') {
		Subir();
	}
	if (dato == 'B') {
		Bajar();
	}
}

void Subir(void) {
	if (R0 <= 15) {
		R0 = R0 + 5;
	}

}
void Bajar(void) {
	if (R0 >= 5) {
		R0 = R0 - 5;
	}
}
void DispararTriac(void) {
	if (R0 >= 0 && R0 <= 20) {
	}
}

/*==================[external data definition]===============================*/
analog_input_config PA = { CH1, AINPUTS_SINGLE_READ, &TakeSample };

/*==================[external functions definition]==========================*/



int main(void) {
	SystemClockInit();
	AnalogInputInit(&PA);

	timer_config timerA_setup = { TIMER_A, 1, InterrupcionAnalogico };
	TimerInit(&timerA_setup);

	serial_config serial = { SERIAL_PORT_PC, 115200, DoPuerto };
	UartInit(&serial);
	while (1) {
 DispararTriac();
	}

	return 0;
}

/*==================[end of file]============================================*/

