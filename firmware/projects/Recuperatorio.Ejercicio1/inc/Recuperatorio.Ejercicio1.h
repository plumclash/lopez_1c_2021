/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * 	En este proyecto se resuelve el inciso 1 del parcial recuperatorio

 *
 *Diseñar e implementar el firmware de un sistema de medición de volumen.
 Suponga que tiene un vaso de 8 cm de diámetro y 15 cm de alto, se coloca
 el sensor de distancia en la parte superior como muestra en la figura:

 Calcule a partir de la distancia medida a la superficie del agua la cantidad de la misma que contiene el vaso.
 Envíe este dato 6 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 Utilice interrupciones de timer. (Para el cálculo de volumen aproxime pi a 3,14).
 *
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Se comienza el proyecto		                 |
 *
 *
 * @author Solana Lopez
 *
 */


#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

