/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * 	En este proyecto se resuelve el inciso 1 del parcial recuperatorio
 *
 *Diseñar e implementar el firmware de un sistema de medición de volumen.
 *Diseñar
 *Diseñar Suponga que tiene un vaso de 8 cm de diámetro y 15 cm de alto, se coloca
 *Diseñar el sensor de distancia en la parte superior como muestra en la figura:

 Calcule a partir de la distancia medida a la superficie del agua la cantidad de la misma que contiene el vaso.
 Envíe este dato 6 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 Utilice interrupciones de timer. (Para el cálculo de volumen aproxime pi a 3,14).
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Se comienza el proyecto		                 |
 *
 *
 * @author Solana Lopez
 *
 */
/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.Ejercicio1.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"

/*==================[macros and definitions]=================================*/
#define DIAMETRO 8
#define ALTO 15
#define PI 3.14

void SisInit(void);
void Volumen(void);
void CalculoDeVolumen(void);

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/* o 6 veces por segundo, es decir 1/6), y como va en mili,166.67*/
timer_config my_timer = { TIMER_A, 166.67, &Volumen };

float volumen = 0;
float altura_cm = 0;


/*==================[external functions definition]==========================*/
void SisInit(void) {
	SystemClockInit();
	TimerInit(&my_timer);
	LedsInit();
	SwitchesInit();
	serial_config mediciones = { SERIAL_PORT_PC, 115200, NULL };
	UartInit(&mediciones);
	TimerStart (TIMER_A);

}

void Volumen(void) {
	CalculoVolumen();
	UartSendString(SERIAL_PORT_PC, UartItoa(volumen, 10));
	UartSendString(SERIAL_PORT_PC, " m3 \n\r");
}

void CalculoVolumen(void) {
	altura_cm = HcSr04ReadDistanceCentimeters();

	/* considero al vaso como un cilindro para poder utilizar la formula de
	 * volumen del cilindro
	 * V= PI.D.H
	 */
	volumen = ((ALTO - altura_cm) * DIAMETRO * PI); //esto esta en cm3

}


int main(void) {
	SisInit();


while (1) {

}

return 0;
}

/*==================[end of file]============================================*/

