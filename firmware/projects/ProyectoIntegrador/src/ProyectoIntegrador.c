///*! @mainpage Proyecto Integrador: Adquisicion de Señal EMG, visualizacion y deteccion de ONSET
// *
// * \section genDesc General Description
// *
// * La aplicacion permite visualizar una señal de EMG, y detectar cuando el individuo cierre el puño para luego utilizarlo para poder
// * controlar una protesis. En este caso es probado en muñeca, dado la simplicidad, pero podria ser tambien en la pierna o pie.
// *
// * \section hardConn Hardware Connection
// *
// * | Dispositivo1	|   EDU-CIAA	|
// * |:--------------:|:--------------|
// * | 	J1 CANAL2	| 	CH1	    	|
// * |    	  VCC	| 	  +5V	    |
// * |   	  GND	 	| 	   GND		|
// *
// *
// * @section changelog Changelog
// *
// * |   Date	    | Description                                    |
// * |:----------:|:-----------------------------------------------|
// * | 05/06/2021 |Se crea proyecto    	                         |
// * | 12/06/2021	|Se termina codificacion			             |
// * | 21/06/2021	|Se calibra Onset		          	             |
// * | 22/06/2021	|Se finaliza proyecto    			             |
// *
// * @author Solana Lopez
// *
// */
/*==================[inclusions]=============================================*/
#include "../inc/ProyectoIntegrador.h"     /* <= own header */
#include "systemclock.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"
#include "math.h"
#include "led.h"

/*==================[macros and definitions]=================================*/
#define ONSET 300

#define PESOPROM 0.98


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
/* Cambia bool de false a true, cuando se preciona la tecla 1*/
	void OnOff(void);
	/*Detecta Onset, enciede un led*/
	void DetectorOnset(void);
	/* calculo alfa para filtro pasa bajo*/
	void CalculoAlfa(void);
	/*Lee Señal que ingresa, y la muestra por pantalla */
	void LeerAnalogico(void);
	/*Procesa señal EMG, la rectifica y le aplica pasa bajos*/
	void ProcesamientoEMG(void);
	/* comienza la conversion de la señal analogica obtenida*/
	void InterrupcionAnalogica(void);
	/*Se encarga de inicializar*/
	void SistInit(void);

/*==================[external data definition]===============================*/
timer_config timerA_setup = { TIMER_A, 1, InterrupcionAnalogica};
analog_input_config AnalogicoEntrada= { CH1, AINPUTS_SINGLE_READ,LeerAnalogico };
uint16_t onset;
uint16_t muestra;
uint16_t EMG=0;
int16_t EMGp;
uint16_t EMGrectificada=0;
uint16_t EMGfiltrada=0;
uint16_t contador;
uint16_t emg_anterior=0;
uint16_t promedio = 0;
uint16_t fc = 1;
bool onoff = false;
/* PARA EL FILTRO*/
float a = 0;
float RC = 0;
float pi= 3.14;


/*==================[external functions definition]==========================*/

void InterrupcionAnalogica(void) {
AnalogStartConvertion();

}

void ProcesamientoEMG(void) { //rectifico
promedio=promedio*PESOPROM+EMG*(1-PESOPROM);
EMGp=EMG-promedio;
if (EMGp < 0)
EMGrectificada = EMGp *(-1);
else
EMGrectificada = EMGp;
/* filtro pasa bajo */

EMGfiltrada = emg_anterior + a * (EMGrectificada - emg_anterior);
emg_anterior = EMGfiltrada;

}
void DetectorOnset(void)
{
if(EMGfiltrada > ONSET)
LedOn(LED_1);

else
LedOff(LED_1);
}

void LeerAnalogico(void) {

if (onoff == true) {
AnalogInputRead(CH1, &muestra);
EMG = muestra * (3300 / 1024); // lo paso a mV;
contador++;
ProcesamientoEMG();
//muestro por osciloscopio las 3.
/* SEÑAL SIN FILTRAR*/
   UartSendString(SERIAL_PORT_PC, UartItoa(EMG, 10));
UartSendString(SERIAL_PORT_PC, ",");
/*SEÑAL RECTIFICADA*/
UartSendString(SERIAL_PORT_PC, UartItoa(EMGrectificada, 10));
UartSendString(SERIAL_PORT_PC, ",");
/* SEÑAL FILTRADA*/
UartSendString(SERIAL_PORT_PC, UartItoa(EMGfiltrada, 10));
            UartSendString(SERIAL_PORT_PC, "\r");



}

//DetectorOnset();
}

/* calculo alfa para filtro pasa bajo*/
void CalculoAlfa(void) {
RC = 1 / (2 * pi * fc);
a = 0.001 / (RC + 0.001);
}

void OnOff(void){
if (onoff == false)
onoff = true;

else
onoff = false;
}
void SistInit(void) {
SystemClockInit();
AnalogInputInit(&AnalogicoEntrada);

/* Configuracion para uso del timer */
TimerInit(&timerA_setup);
TimerStart(TIMER_A);

/* SERIAL COMUNICATION */

serial_config serialPort = { SERIAL_PORT_PC, 115200, NULL };

UartInit(&serialPort);

CalculoAlfa();

/* Inicializo teclas*/
SwitchesInit();

SwitchActivInt(SWITCH_1, OnOff);
LedsInit();

}
int main(void){
SistInit();
    while(1){
DetectorOnset();
}

return 0;
}

/*==================[end of file]============================================*/
