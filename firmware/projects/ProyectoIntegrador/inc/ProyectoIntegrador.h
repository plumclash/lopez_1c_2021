///*! @mainpage Proyecto Integrador: Adquisicion de Señal EMG, visualizacion y deteccion de ONSET
// *
// * \section genDesc General Description
// *
// * La aplicacion permite visualizar una señal de EMG, y detectar cuando el individuo cierre el puño para luego utilizarlo para poder
// * controlar una protesis. En este caso es probado en muñeca, dado la simplicidad, pero podria ser tambien en la pierna o pie.
// *
// * \section hardConn Hardware Connection
// *
// * | Dispositivo1	|   EDU-CIAA	|
// * |:--------------:|:--------------|
// * | 	J1 CANAL2	| 	CH1	    	|
// * |    	  VCC	| 	  +5V	    |
// * |   	  GND	 	| 	   GND		|
// *
// *
// * @section changelog Changelog
// *
// * |   Date	    | Description                                    |
// * |:----------:|:-----------------------------------------------|
// * | 05/06/2021 |Se crea proyecto    	                         |
// * | 12/06/2021	|Se termina codificacion			             |
// * | 21/06/2021	|Se calibra Onset		          	             |
// * | 22/06/2021	|Se finaliza proyecto    			             |
// *
// * @author Solana Lopez
// *
// */
#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

