/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2020 | Proyecto creado		                         |
 * | 28/05/2020	|							                     |
 *
 * @author Solana
 *

 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.ECG.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define BUFFER_SIZE 231

/*==================[internal data definition]===============================*/

uint16_t valor;

const char ecg[BUFFER_SIZE] = { 76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91, 99, 105, 101, 96, 102,
		106, 101, 96, 100, 107, 101, 94, 100, 104, 100, 91, 99, 103, 98, 91, 96,
		105, 95, 88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80, 83, 92,
		86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82, 79, 69, 80, 82, 81, 70,
		75, 81, 77, 74, 79, 83, 82, 72, 80, 87, 79, 76, 85, 95, 87, 81, 88, 93,
		88, 84, 87, 94, 86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144, 180, 210, 236,
		253, 227, 171, 99, 49, 34, 29, 43, 69, 89, 89, 90, 98, 107, 104, 98,
		104, 110, 102, 98, 103, 111, 101, 94, 103, 108, 102, 95, 97, 106, 100,
		92, 101, 103, 100, 94, 98, 103, 96, 90, 98, 103, 97, 90, 99, 104, 95,
		90, 99, 104, 100, 93, 100, 106, 101, 93, 101, 105, 103, 96, 105, 112,
		105, 99, 103, 108, 99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88,
		77, 69, 75, 79, 74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77,
		77, 76, 76, };

uint16_t cont = 0;
uint16_t dato = 0;
uint16_t contador = 1;
uint16_t fc = 10;
float a = 0;
float RC = 0;
uint16_t salida_filtrada_anterior = 0;
uint16_t salida_filtrada = 0;
uint16_t entrada = 0;
float pi = 3.14;
bool modo = false;

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
void Interrupcion(void) {
	AnalogStartConvertion();
}
void ConversionAD(void) {

	AnalogInputRead(CH1, &dato);
	if (modo == false) {
		UartSendString(SERIAL_PORT_PC, UartItoa(dato, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
	} else {

		salida_filtrada = salida_filtrada_anterior
				+ a * (dato - salida_filtrada_anterior);
		salida_filtrada_anterior = salida_filtrada;
		// aca debo poner el filtro, cambiando a dato
		UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada, 10));
		UartSendString(SERIAL_PORT_PC, "\r");

	}

	if (contador == 2) { // contador para que alterne de 2 a 4 el timer, con una sola interrupcion
		AnalogOutputWrite(ecg[cont]);
		contador = 1;
		cont++;

		if (cont == BUFFER_SIZE) {
			cont = 0;
			contador = 0;
		}

	}

	contador++;
}

void OnFiltro(void) {
	modo = true;

}

void OffFiltro(void) {
	modo = false;

}

void SubirFrec(void) {

	if(fc<80)
	fc = fc + 5;


	Frecuencia();

}
void BajarFrec(void) {
	if(fc>5)
	fc = fc - 5;

	Frecuencia();

}

void Frecuencia(void) {
	RC = 1 / (2 * pi * fc);
	a = 0.002 / (RC + 0.002);
}

/*==================[external functions definition]==========================*/

int main(void) {

	SystemClockInit();
	serial_config UART_USB = { SERIAL_PORT_PC, 115200, NULL };

	analog_input_config ConversorAD = { CH1, AINPUTS_SINGLE_READ, ConversionAD };

	UartInit(&UART_USB);

	AnalogInputInit(&ConversorAD);
	AnalogOutputInit();

	timer_config timerA_setup = { TIMER_A, 2, Interrupcion };
	TimerInit(&timerA_setup);

	TimerStart(TIMER_A);

	SwitchesInit();
	Frecuencia();
	SwitchActivInt(SWITCH_1, OnFiltro);
	SwitchActivInt(SWITCH_2, OffFiltro);
	SwitchActivInt(SWITCH_3, BajarFrec);
	SwitchActivInt(SWITCH_4, SubirFrec);

	while (1) {

	}

	return 0;
}



/*==================[end of file]============================================*/

