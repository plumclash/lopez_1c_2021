/*! @mainpage DisplayITS_E0803
 *
 * \section genDesc Descripcion General
 * un sistema embebido que controla una interfaz de salida LCD de 3 dígitos
 *
 *
 *
 *
 * \section hardConn  Conexion de Hardware
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1       	| 	LCD1		|
 * | 	D2   	 	| 	LCD2		|
 * | 	D3        	| 	LCD3		|
 * | 	D4   	 	| 	LCD4		|
 * | 	SEL_0      	| 	GPIO1		|
 * | 	SEL_1      	| 	GPIO3   	|
 * | 	SEL_2      	| 	GPIO5		|
 * |     +5V      	| 	+5V 		|
 * | 	GND      	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * |/05/05/2021 | Creacion documento		                     |
 *				 |
 *
 * @autor Solana Lopez Aguero
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H



/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif



bool ITSE0803Init(gpio_t * pins);
bool ITSE0803DisplayValue(uint16_t valor);
uint16_t ITSE0803ReadValue(void);
bool ITSE0803Deinit(gpio_t * pins);
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

