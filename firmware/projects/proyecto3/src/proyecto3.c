/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto3.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define ON 1
#define OFF 0

/*==================[internal data definition]===============================*/
bool on_off = false;
bool hold = false;
bool send = false;

/*==================[internal functions declaration]=========================*/
void OnOff(void) {
	if (on_off == true) {
		on_off = false;
	} else {
		on_off = true;
	}
	hold = false;
}

void Hold(void) {
	if (hold == true) {
		hold = false;
	} else {
		hold = true;
	}
}

void InterrupcionT(void) {
	send = true;
}
void DoPuerto(void) {
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	if (dato == 'h') {
		Hold();
	}
	if (dato == 'o') {
		OnOff();
	}
}

void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	SystemClockInit();
	gpio_t pins[7] = { GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1,
			GPIO_3, GPIO_5 };

	ITSE0803Init(pins);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();
	SwitchesInit();

	SwitchActivInt(SWITCH_1, OnOff);
	SwitchActivInt(SWITCH_2, Hold);
	uint16_t medida_cm = 0;
	//	uint16_t medida_in=0;

	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = DoPuerto;
	UartInit(&UART_USB);
	timer_config timerA_setup = { TIMER_A, 1000, InterrupcionT };
	TimerInit(&timerA_setup);

	TimerStart(TIMER_A);

	while (1) {

		if (on_off == 1) {
			medida_cm = HcSr04ReadDistanceCentimeters();
			if ((hold) == 0) {
				ITSE0803DisplayValue(medida_cm);

				UartSendString(SERIAL_PORT_PC, UartItoa(medida_cm, 10));
				UartSendString(SERIAL_PORT_PC, "cm  \r\n ");

			}
		} else {
			ITSE0803DisplayValue(0);
		}

		DelayMs(500);
	}
}

/*==================[end of file]============================================*/







/*==================[end of file]============================================*/
