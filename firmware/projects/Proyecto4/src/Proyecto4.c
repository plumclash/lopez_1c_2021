/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Solana
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define BUFFER_SIZE 231

/*==================[internal data definition]===============================*/

 uint16_t valor;

 const char ecg[BUFFER_SIZE]={
 		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
 		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
 		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
 		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
 		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
 		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
 		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
 		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
 		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
 		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
 		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
 		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
 		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
 		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
 		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
 		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
 		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
 };


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

void ConversionAD(void){

	AnalogInputReadPolling(CH1,&valor);
	UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10));
	UartSendString(SERIAL_PORT_PC, "\r");



}

/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	serial_config UART_USB={SERIAL_PORT_PC,115200, NULL};

	analog_input_config ConversorAD={CH1,AINPUTS_SINGLE_READ,NULL};

	UartInit(&UART_USB);


	AnalogInputInit(&ConversorAD);



	timer_config timerA_setup = {TIMER_A,2,ConversionAD};
	TimerInit(&timerA_setup);

	TimerStart(TIMER_A);


    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

