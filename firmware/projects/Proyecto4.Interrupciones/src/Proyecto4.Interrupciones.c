/*! @mainpage PROYECTO4.INTERRUPCIONES
 *
 * \section genDesc General Description
 *
 * Esta aplicacion muestra por pantalla los valores convertidos mediante interrupciones, la misma aplicacion
 * que el proyecto 4, pero utilizando interrupciones
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Proyecto creado     	                         |
 * |			|							                     |
 *
 * @author Solana
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.Interrupciones.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

 uint16_t valor;


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/
void Interrupcion(void)
{AnalogStartConvertion();
}
void ConversionAD(void){

	AnalogInputRead(CH1,&valor);
	UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10));
	UartSendString(SERIAL_PORT_PC, "\r");



}

/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	serial_config UART_USB={SERIAL_PORT_PC,115200, NULL};

	analog_input_config ConversorAD={CH1,AINPUTS_SINGLE_READ,
			ConversionAD};

	UartInit(&UART_USB);


	AnalogInputInit(&ConversorAD);



	timer_config timerA_setup = {TIMER_A,2, Interrupcion};
	TimerInit(&timerA_setup);

	TimerStart(TIMER_A);


    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

