/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*/Sobre una variable de 32 bits sin signo previamente declarada
y de valor desconocido, asegúrese de colocar el bit 3 a 1 y
los bits 13 y 14 a 0 mediante máscaras y el operador <<.*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

/*==================[macros and definitions]=================================*/



/*==================[internal functions declaration]=========================*/

typedef struct {
	uint8_t port;
	uint8_t pin;
	uint8_t dir;
} gpioConf_t;
void Puerto (uint8_t num, gpioConf_t * vector_struct)
{


								/* PUERTO 1.4 */
	if (num & 1)
	{
		printf("Poner puerto %d . %d en ALTO \r\n", vector_struct[0].port,vector_struct[0].pin);
	}
	else {printf("Poner puerto %d . %d en BAJO \r\n", vector_struct[0].port,vector_struct[0].pin);}

								/* PUERTO 1.5 */
	if (num & 2)
		{
			printf("Poner puerto %d . %d en ALTO \r\n", vector_struct[1].port,vector_struct[1].pin);
		}
	else {printf("Poner puerto %d . %d en BAJO \r\n", vector_struct[1].port,vector_struct[1].pin);}

								/* PUERTO 1.6 */
	if (num & 4)
		{
			printf("Poner puerto %d . %d en ALTO \r\n", vector_struct[2].port,vector_struct[2].pin);
		}
	else {printf("Poner puerto %d . %d en BAJO \r\n", vector_struct[2].port,vector_struct[2].pin);}

								/* PUERTO 2.14 */
	if (num & 8)
		{
			printf("Poner puerto %d . %d en ALTO \r\n", vector_struct[3].port,vector_struct[3].pin);
		}
	else {printf("Poner puerto %d . %d en BAJO \r\n", vector_struct[3].port,vector_struct[3].pin);}


}

int main(void)
{
	gpioConf_t vec_struc [] = {{1,4,1},{1,5,1},{1,6,1},{2,14,1}};

uint8_t b=5;
			return 0;

}
/*==================[end of file]============================================*/

