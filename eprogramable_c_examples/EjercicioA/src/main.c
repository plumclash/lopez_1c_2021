/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*/Sobre una variable de 32 bits sin signo previamente declarada
y de valor desconocido, asegúrese de colocar el bit 3 a 1 y
los bits 13 y 14 a 0 mediante máscaras y el operador <<.*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include <stdio.h>
#include "math.h"

/*==================[macros and definitions]=================================*/



/*==================[internal functions declaration]=========================*/
#define led1 1
#define led2 2
#define led3 3
#define on 1
#define off 2
#define toggle 3

typedef struct {
	uint8_t n_led, n_ciclos, periodo, mode;
} my_leds;
void LedsControl (my_leds *puntero){

uint8_t i=0, j;
	switch ( puntero->mode )
	{
	case on:
		switch (puntero->n_led)
		{
		case led1:
			printf("Poner el led %d en ALTO \r\n", puntero->n_led);
			break;
		case led2:
			printf("Poner el led %d en ALTO \r\n", puntero->n_led);
			break;
		case led3:
			printf("Poner el led %d en ALTO \r\n", puntero->n_led);
			break;
		}
	break;
	case off:
		switch (puntero->n_led)
		{
		case led1:
			printf("Poner el led %d en BAJO \r\n", puntero->n_led);
			break;
		case led2:
			printf("Poner el led %d en BAJO \r\n", puntero->n_led);
			break;
		case led3:
				printf("Poner el led %d en BAJO \r\n", puntero->n_led);
				break;
				}
	break;
	case toggle:

		for(i;i<puntero->n_ciclos;i++)
		{
			switch (puntero->n_led)
					{
					case led1:
						printf("TOGGLEAR LED %d \r\n", puntero->n_led);
						/*if (led1!=0){
						printf("Poner el led %d en BAJO \r\n", puntero->n_led);
						}
						else {printf("Poner el led %d en ALTO \r\n", puntero->n_led);}
						*/
						break;
					case led2:
						printf("TOGGLEAR LED %d \r\n", puntero->n_led);
						/*if (led2!=0){
						printf("Poner el led %d en BAJO \r\n", puntero->n_led);
						}
						else {printf("Poner el led %d en ALTO \r\n", puntero->n_led);}
						*/
						break;
					case led3:
						printf("TOGGLEAR LED %d \r\n", puntero->n_led);
						/*if (led3!=0){
						printf("Poner el led %d en BAJO \r\n", puntero->n_led);
						}
						else {printf("Poner el led %d en ALTO \r\n", puntero->n_led);}
						*/
						break;
					} //switch toggle

			for (j=0;j<puntero->periodo;j++)
			{
				// periodo de espera para el siguiente ciclo, revisar como llega el dato periodo
			}


		} //for
	break;
	}//switch mode
}
int main(void)
{my_leds LEDS[]={{1,1,1,1}};


LedsControl (&LEDS);


return 0;}



/*==================[end of file]============================================*/

