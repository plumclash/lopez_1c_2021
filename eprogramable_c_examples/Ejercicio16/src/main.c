/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*/Sobre una variable de 32 bits sin signo previamente declarada
y de valor desconocido, asegúrese de colocar el bit 3 a 1 y
los bits 13 y 14 a 0 mediante máscaras y el operador <<.*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/



/*==================[internal functions declaration]=========================*/

typedef union {

	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
	uint32_t DATO;

} fraccionamiento;
int main(void)
{ //incisoA

	uint32_t Datos=0x01020304;

	uint8_t aa=0x0;
	uint8_t bb=0x0;
	uint8_t cc=0x0;
	uint8_t dd=0x0;


	dd = Datos;

	cc = Datos >> 8;

	bb = Datos >> 16;

	aa = Datos >> 24;

	printf("\nDatos: %d - %d - %d - %d", aa, bb, cc, dd);

						// Consigna 16 - b
	fraccionamiento bytes;

	bytes.DATO=0x01020304;
	bytes.byte1= bytes.DATO>>24;
	printf("\nDatos: %d", bytes.byte1);


			return 0;

}
/*==================[end of file]============================================*/

