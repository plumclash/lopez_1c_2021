/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/
typedef struct {
	char nombre [12];
	char apellido [20];
	uint8_t edad;
}Alumno_t;
//siempre con t ;



/*==================[internal functions declaration]=========================*/



int main(void)
{ //incisoA.
	Alumno_t prueba;
	uint8_t i;
	char nombre1[]= {'S','o','l','a','n','a','\0'};
for(i=0; i<12;i++){
	prueba.nombre[i]= nombre1[i];
}
char apellido1[]= "LOPEZ";
for(i=0;i<20;i++)
{prueba.apellido[i]= apellido1[i];
}
prueba.edad=22;
printf("Nombre: %s \nAPELLIDO: %s \nEDAD: %s",prueba.nombre,prueba.apellido,prueba.edad);
//incisoB
Alumno_t *comp;
Alumno_t E ={"Gonzalo", "CEPEDA","21"};
comp=&E;


printf("\nNombre: %s \nAPELLIDO: %s", comp->nombre, comp->apellido,comp->edad);


	return 0;
}

/*==================[end of file]============================================*/

